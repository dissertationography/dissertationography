Marx, K. (~1939). "The Fragment on Machines" from The Grundrisse.

-three parts in capital: material of labour, means of labour, living labour. "On one side, capital was divided into these three elements in accordance with its material composition; on the other, the //labour process// (or the merging of these elements into each other within the process) was their moving unity, the product their static unity. In this form, the material elements--material of labour, means of labour and living labour--appeared merely as the essential moments of the labour process itself, which capital appropriates" (691)

-
