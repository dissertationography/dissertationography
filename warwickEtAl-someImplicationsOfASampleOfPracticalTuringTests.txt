Warwick, K., Shah, H. & Moor, J. (2013). Some implications of a sample of practical Turing Tests. Minds & Machines 23, pp. 163-177. 

In which the first author is consistently taken for a non-human while participating in Turing Tests on the anniversary of the birth of Alan Turing.

"Turing originally proposed the test as a replacement for the question 'Can Machines Think?' (Turing 1950). This point is critical in this paper." (164)

"a witness, human or machine does not have to provide an elaborate answer in the test, but whether it is enough to satisfy the interrogator is subjective." (165)

"Both the interrogators and hidden humans involved in such tests are (importantly) //human//. Humans have lots of different ways of going about a task." (165)

"Whilst, as a basic requirement of such tests, the aim of the machines involved is to converse in a human-like manner, the requirement for a hidden human is simply to be themselves, essentially human." (166)

"In a Turing test we assume the hidden human’s goal is to be identified as a human and they will take positive steps to make sure his humanity stands out. However this requires cooperation in the linguistic exchange with the interrogator." (172)

"For the interrogator the goal is to identify humans as humans and machines as machines. The interrogating judge can increase his or her chances of accomplishing this goal by asking questions he or she thinks many humans would know readily but machines probably would not." (172)


