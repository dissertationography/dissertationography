Star, S.L. (2010). This is Not a Boundary Object: Reflections on the Origin of a Concept. Science, Technology, & Human Values, 35, 5, 601-617. 

-"Boundary objects are a sort of arrangement that allow different groups to work together without consensus" (602)
-in relations between users and developers, "What was obvious to one was a mystery to another. What was trivial to one was a barrier to another. Yet, clarifying this was never easy. The users like the interface when they were sat in front of it. Yet, they did not know how to make a reliable working infrastructure out of it." (610)
-
