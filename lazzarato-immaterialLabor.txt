Lazzarato, M. (?) "Immaterial Labour"

-"//immaterial labor//, which is defined as the labor that produces the informational and cultural content of the commodity" 
-"as regards the 'informational content' of the commodity, it refers directly to the changes taking place in workers' labor processes in big companies in the industrial and tertiary sectors, where the skills involved in direct labor are increasingly skills involving cybernetics and computer control"
-"immaterial labor involves a series of activities that are not normally recognized as 'work'--in other words, the kinds of activities involved in defining and fixing cultural and artistic standards, fashions, tastes, consumer norms, and, more strategically, public opinion"
-
