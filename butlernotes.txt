Butler, J. (1999). Gender trouble: Femminism and the subversion of identity. London ; New York, N.Y: Routledge. 

-"If gender is no longer to be understood as consolidated through normative sexuality, then is there a crisis of gender that is specific to queer contexts?" (xi)
-the notion of linking different sides of life: Butler makes a point of having her non-academic life matter in her academic life (xvii)
-"proper and improper masculinity and femininity" (xxiii)
-"laughter in the face of serious categories is indispensable for feminism" (xxviii)
-"there is the political problem that feminism encounters in the assumption that the term //women// denotes a common identity" (6)
-is "woman" an oppositional concept? (7)
-by making feminism a politics of women, the need to draw boundaries around woman arises (9)
-if we break down the sex/gender link, then what becomes of the assignment of gender to people of varying sexes? (10)
-who gives a given gender? (10)
-do we have gender or are we gendered? (11)
-the construction of gender requires a human constructor (12)
-construction implies the conflict between "free will and determinism" (12)
-making "woman" a clear category is problematic (19)
-if the concept of identity is a construction of multiple parts, how do we define identity when the stability of the parts is called into question? (23)

stopped at 24, because book was recalled from library. need to buy a copy or something.
